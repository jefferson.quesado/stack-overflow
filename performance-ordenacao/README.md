Feito para responder a dúvida do [João Paulo Rolim](https://pt.stackoverflow.com/q/235616/64969).

A ideia aqui foi:

1. criar uma base para poder fazer testes estatísticos com repetição
2. obter as saídas dos testes
3. verificar como tornar mais eficiente a ordenação do merge sort
