package sort;

import java.util.Random;

public class Array {
	private final int v[];
	private final int n;

	public static Array getRandomArray(int n) {
		Array a = new Array(n);

		Random number = new Random();

		for (int i = 0; i < n; i++) {
			// Random.nextInt() é, pelo que li, uniforme em todo o domínio
			// inteiro 32 bits
			a.v[i] = number.nextInt();
		}

		return a;
	}

	public Array(int n) {
		v = new int[n];
		this.n = n;
	}

	public void bubbleSort() {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n - 1; j++) {
				if (v[j] > v[j + 1]) {
					change(j, j + 1);
				}
			}
		}
	}

	public void bubbleSort2() {
		for (int i = n - 1; i >= 0; i--) {
			for (int j = 0; j < i - 1; j++) {
				if (v[j] > v[j + 1]) {
					change(j, j + 1);
				}
			}
		}
	}

	public void bubbleSort2flagged() {
		boolean houveTroca;
		for (int i = n - 1; i >= 0; i--) {
			houveTroca = false;
			for (int j = 0; j < i - 1; j++) {
				if (v[j] > v[j + 1]) {
					change(j, j + 1);
					houveTroca = true;
				}
			}
			if (!houveTroca) {
				return;
			}
		}
	}

	private void change(int pos1, int pos2) {
		int aux = v[pos1];
		v[pos1] = v[pos2];
		v[pos2] = aux;
	}

	public void selectionSort() {
		int min;
		for (int i = 0; i < n - 1; i++) {
			min = i;
			for (int j = i + 1; j < n; j++) {
				if (v[j] < v[min]) {
					min = j;
				}
			}
			change(i, min);
		}
	}

	public void insertionSort() {
		int i, j;
		int temp;
		for (i = 1; i < n; i++) {
			temp = v[i];
			j = i;
			while ((j > 0) && (v[j - 1] > temp)) {
				v[j] = v[j - 1];
				j = j - 1;
			}
			v[j] = temp;
		}
	}

	public void mergeSort() {
		mergeSort(0, n - 1);
	}

	/**
	 * ordenar o vetor no intervalo [ini,end], fechado em ambos os lados
	 * 
	 * @param ini
	 * @param end
	 */
	private void mergeSort(int ini, int end) {

		if (ini >= end) {
			return;
		}

		int middle = (ini + end) / 2;
		mergeSort(ini, middle);
		mergeSort(middle + 1, end);
		merge(ini, middle, end);
	}

	private void merge(int start, int middle, int end) {
		int[] aux = new int[v.length];

		for (int x = start; x <= end; x++) {
			aux[x] = v[x];
		}

		int i = start;
		int j = middle + 1;
		int k = start;

		while (i <= middle && j <= end) {
			if (aux[i] < aux[j]) {
				v[k++] = aux[i++];
			} else {
				v[k++] = aux[j++];
			}
		}

		while (i <= middle) {
			v[k++] = aux[i++];
		}

		while (j <= end) {
			v[k++] = aux[j++];
		}
	}

	public void mergeSort2() {
		mergeSort2(0, n - 1);
	}

	/**
	 * ordenar o vetor no intervalo [ini,end], fechado em ambos os lados
	 * 
	 * @param ini
	 * @param end
	 */
	private void mergeSort2(int ini, int end) {
		if (ini >= end) {
			return;
		}

		int middle = (ini + end) / 2;
		mergeSort2(ini, middle);
		mergeSort2(middle + 1, end);
		merge2(ini, middle, end);
	}

	public void merge2(int start, int middle, int end) {
		int[] helper = new int[v.length];

		for (int i = start; i <= end; i++) {
			helper[i] = v[i];
		}

		int i = start;
		int j = middle + 1;
		int k = start;

		while (i <= middle && j <= end) {
			if (helper[i] <= helper[j]) {
				v[k] = helper[i];
				i++;
			} else {
				v[k] = helper[j];
				j++;
			}
			k++;
		}

		while (i <= middle) {
			v[k] = helper[i];
			k++;
			i++;
		}
	}

	public void mergeSort3() {
		// cria o vetor auxiliar uma única vez
		int aux[] = new int[n];

		System.arraycopy(v, 0, aux, 0, n);

		mergeSort3(aux, v, 0, n);
	}

	/**
	 * ordenar o vetor no intervalo [ini,end), fechado no começo e aberto no
	 * final
	 * 
	 * areaTrabalho vai carregar o etor ordenado, no final
	 * 
	 * @param entradaDesordenada
	 *            vetor de entrada, não prometo alterar
	 * @param areaTrabalho
	 *            local onde vai acontecer o trabalho de ordenação das
	 *            informações da entradaDesordenada
	 * @param ini
	 * @param end
	 */
	private void mergeSort3(int[] entradaDesordenada, int[] areaTrabalho, int ini, int end) {
		if (end - ini <= 1) {
			return;
		}

		int middle = (ini + end) / 2;
		mergeSort3(areaTrabalho, entradaDesordenada, ini, middle);
		mergeSort3(areaTrabalho, entradaDesordenada, middle, end);
		merge3(entradaDesordenada, areaTrabalho, ini, middle, end);
	}

	public void merge3(int[] entradaDesordenada, int[] areaTrabalho, int start, int middle, int end) {
		int i = start;
		int j = middle;

		for (int k = start; k < end; k++) {
			if (i < middle && (j >= end || entradaDesordenada[i] <= entradaDesordenada[j])) {
				areaTrabalho[k] = entradaDesordenada[i];
				i++;
			} else {
				areaTrabalho[k] = entradaDesordenada[j];
				j++;
			}
		}
	}
}
