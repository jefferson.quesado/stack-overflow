package sort;

import java.util.function.Consumer;
import java.util.function.LongSupplier;

public class SortStuff {
	public static final int SIZE_ARRAY = 30_000;
	public static final int N_REPETITION = 100;
	
	public static LongSupplier calculaTempo(Consumer<Array> metodoOrdenacao) {
		return () -> {
			Array a = Array.getRandomArray(SIZE_ARRAY);
			
			long init = System.currentTimeMillis();
			metodoOrdenacao.accept(a);
			return System.currentTimeMillis() - init;
		};
	}

	public static void main(String[] args) {
		Estatisticas estatBubble = repeatGetEstatisticas(N_REPETITION, calculaTempo(Array::bubbleSort));
		Estatisticas.printEstatisticas(estatBubble, "BUBBLE SORT");
		
		Estatisticas estatBubble2 = repeatGetEstatisticas(N_REPETITION, calculaTempo(Array::bubbleSort2));
		Estatisticas.printEstatisticas(estatBubble2, "BUBBLE SORT MINIMO");
		
		Estatisticas estatBubble2flagged = repeatGetEstatisticas(N_REPETITION, calculaTempo(Array::bubbleSort2flagged));
		Estatisticas.printEstatisticas(estatBubble2flagged, "BUBBLE SORT FLAGGED");
		
		Estatisticas estatSelection = repeatGetEstatisticas(N_REPETITION, calculaTempo(Array::selectionSort));
		Estatisticas.printEstatisticas(estatSelection, "SELECTION SORT");
		
		Estatisticas estatInsertion = repeatGetEstatisticas(N_REPETITION, calculaTempo(Array::insertionSort));
		Estatisticas.printEstatisticas(estatInsertion, "INSERTION SORT");
		
		Estatisticas estatMerge = repeatGetEstatisticas(N_REPETITION, calculaTempo(Array::mergeSort));
		Estatisticas.printEstatisticas(estatMerge, "MERGE SORT");
		
		Estatisticas estatMerge2 = repeatGetEstatisticas(N_REPETITION, calculaTempo(Array::mergeSort2));
		Estatisticas.printEstatisticas(estatMerge2, "MERGE SORT (2)");
		
		Estatisticas estatMerge3 = repeatGetEstatisticas(N_REPETITION, calculaTempo(Array::mergeSort3));
		Estatisticas.printEstatisticas(estatMerge3, "MERGE SORT (3)");
	}

	private static Estatisticas repeatGetEstatisticas(int n, LongSupplier l) {
		Estatisticas estat = new Estatisticas();
		for (int i = 0; i < n; i++) {
			estat.adicionaRodada(l.getAsLong());
			System.err.println("Terminou a rodada " + i);
		}
		
		return estat;
	}

}
